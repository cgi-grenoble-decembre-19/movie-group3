import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UiService {
  private openP$ = new BehaviorSubject(false);
  public open$ = this.openP$.asObservable();
  constructor() { }

  toggle() {
    const val = !this.openP$.value;
    this.openP$.next(val);
  }
}
