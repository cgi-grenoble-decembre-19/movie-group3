import { Component, OnInit } from '@angular/core';
import { UiService } from '../../services/ui.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  textButton = 'Menu';
  constructor(private uiService: UiService) { }

  ngOnInit() {
  }
  toggle() {
    this.uiService.toggle();
  }
}
