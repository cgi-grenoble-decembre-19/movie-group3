import { Component, OnInit } from '@angular/core';
import { UiService } from './services/ui.service';

@Component({
  selector: 'app-ui',
  templateUrl: './ui.component.html',
  styleUrls: ['./ui.component.scss']
})
export class UiComponent implements OnInit {
  open: boolean;

  constructor(private uiService: UiService) {
    this.uiService.open$.subscribe(open =>
      this.open = open
    );
  }

  ngOnInit() {
  }

}
