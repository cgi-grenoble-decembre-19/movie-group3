import { Component, OnInit, Input } from '@angular/core';
import { Movie } from 'src/app/shared/models/movie';
import { Router } from '@angular/router';

@Component({
  selector: 'app-movie-card',
  templateUrl: './movie-card.component.html',
  styleUrls: ['./movie-card.component.scss']
})
export class MovieCardComponent implements OnInit {
 @Input() movie: Movie = new Movie();
  constructor(private router: Router) { }

  ngOnInit() {
  }

  btnClick() {
    this.router.navigate(['/movies', 'id']);
  }
}
