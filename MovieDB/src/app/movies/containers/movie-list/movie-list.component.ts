import { MovieService } from './../../services/movie.service';
import { Component, OnInit } from '@angular/core';
import { Movie } from 'src/app/shared/models/movie';

@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.scss']
})
export class MovieListComponent implements OnInit {
  movies: Movie[];
  constructor(private movieService: MovieService) {
    this.movieService.getPopularMovies().subscribe(data => {
      this.movies = data['results'];});
  }


  ngOnInit() {
  }

}
