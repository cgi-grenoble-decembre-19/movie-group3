import { MaterialModule } from './../material/material.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common'; import { MovieCardComponent } from './containers/movie-card/movie-card.component';
import { MovieListComponent } from './containers/movie-list/movie-list.component';
import { MovieDetailsComponent } from './containers/movie-details/movie-details.component';
import { Routes, RouterModule } from '@angular/router';



const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  { path: 'details/:id', component: MovieDetailsComponent },
  { path: 'list', component: MovieListComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MoviesRoutingModule { }

