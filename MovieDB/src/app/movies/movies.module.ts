import { MaterialModule } from './../material/material.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MovieCardComponent } from './containers/movie-card/movie-card.component';
import { MovieListComponent } from './containers/movie-list/movie-list.component';
import { MovieDetailsComponent } from './containers/movie-details/movie-details.component';
import { MoviesRoutingModule } from './movies-routing.module';



@NgModule({
  declarations: [MovieCardComponent, MovieListComponent, MovieDetailsComponent],
  imports: [
    CommonModule,
    MaterialModule,
    MoviesRoutingModule,
  ],
  exports: [MoviesRoutingModule]
})
export class MoviesModule { }
