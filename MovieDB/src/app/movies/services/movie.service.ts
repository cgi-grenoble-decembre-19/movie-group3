import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MovieService {

  private movieUrl = 'https://api.themoviedb.org/3/';
  private apiKey = '6cc255f4cc2333d4f7e0ea32aa24d80e';
  private movieString: string;
  private id: number;

  constructor(public _http: HttpClient) { }

  searchMovie(movie: string) {
    this.movieString = movie;
    return this._http.get(this.movieUrl + 'search/movie?query=' + this.movieString + '&apiKey=' + this.apiKey);
  }

  getUpcomingMovies() {
    // tslint:disable-next-line:max-line-length
    return this._http.get(this.movieUrl + 'discover/movie?primary_release_date.gte=2018-04-15&primary_release_date.lte=2018-07-31' + '&apiKey=' + this.apiKey);
  }

  getPopularMovies() {
    return this._http.get(this.movieUrl + 'discover/movie?sort_by=popularity.desc' + '&apiKey=' + this.apiKey);
  }

  getMovie(id: number) {
    return this._http.get(this.movieUrl + 'movie/' + id + '?apiKey=' + this.apiKey);
  }

}


// Get popular movies api
// https://api.themoviedb.org/3/discover/movie?sort_by=popularity.desc&apiKey=apiKey

// Get upcoming movies api
// tslint:disable-next-line:max-line-length
// https://api.themoviedb.org/3/discover/movie?primary_release_date.gte=2018-04-15&primary_release_date.lte=2018-07-31&apiKey=apiKey

// Search movies api
// https://api.themoviedb.org/3/search/movie?query=SEARCH_STRING&apiKey=apiKey

// Get movies api
// https://api.themoviedb.org/3/movie/ID?apiKey=apiKey
