import { ActorI } from './../interfaces/actor-i';
export class Actor implements ActorI {
  idActor: number;  name: string;
  profilePath: string;
  caracter: string;

  constructor(fields?: Partial<Actor>
    // & Pick<Commande, 'price'> --> rend obligatoire de mettre le prix dans le constructeur
  ) {
    if (fields) {
      Object.assign(this, fields);
    }
  }

}
