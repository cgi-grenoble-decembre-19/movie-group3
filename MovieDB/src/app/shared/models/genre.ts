import { GenreI } from './../interfaces/genre-i';
export class Genre implements GenreI {
  idGenre: number;
  name: string;

  constructor(fields?: Partial<Genre>
    // & Pick<Commande, 'price'> --> rend obligatoire de mettre le prix dans le constructeur
  ) {
    if (fields) {
      Object.assign(this, fields);
    }
  }
}
