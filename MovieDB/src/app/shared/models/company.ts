import { CompanyI } from './../interfaces/company-i';
export class Company implements CompanyI {
  idCompany: number;  logoPath: string;
  name: string;
  originCountry: string;

  constructor(fields?: Partial<Company>
    // & Pick<Commande, 'price'> --> rend obligatoire de mettre le prix dans le constructeur
  ) {
    if (fields) {
      Object.assign(this, fields);
    }
  }

}
