import { Actor } from './actor';
import { Genre } from './genre';
import { MovieI } from './../interfaces/movie-i';
import { Company } from './company';
export class Movie implements MovieI {
  id: number;
  posterPath: string;
  title: string;
  overview: string;
  releaseDate: string;
  genres: Genre[];
  productionCompanies: Company[];
  rating: number;
  casting: Actor[];

  constructor(fields?: Partial<Movie>
    // & Pick<Commande, 'price'> --> rend obligatoire de mettre le prix dans le constructeur
  ) {
    if (fields) {
      Object.assign(this, fields);
    }
  }

}
