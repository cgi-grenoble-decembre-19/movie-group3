import { Genre } from '../models/genre';
import { Company } from '../models/company';
import { Actor } from '../models/actor';

export interface MovieI {
  id: number;
  posterPath: string;
  title: string;
  overview: string;
  releaseDate: string;
  genres: Genre[];
  productionCompanies: Company[];
  rating: number;
  casting: Actor[];
}
