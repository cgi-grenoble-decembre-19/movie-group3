export interface ActorI {
  idActor: number;
  name: string;
  profilePath: string;
  caracter: string;
}
