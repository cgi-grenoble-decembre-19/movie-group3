export interface CompanyI {
  idCompany: number;
  logoPath: string;
  name: string;
  originCountry: string;
}
